package ru.nlmk.study.practice.model.enums;

public enum Item {
    STUDENT("student"),
    STUDY_CLASS("class"),
    CITY("city");

    private String itemAlias;

    Item(String itemAlias) {
        this.itemAlias = itemAlias;
    }

    public static Item findByAlias(String itemAlias) {
        for (Item v : values()) {
            if (v.getItemAlias().equals(itemAlias)) {
                return v;
            }
        }
        return null;
    }

    public String getItemAlias() {
        return itemAlias;
    }
}
