package ru.nlmk.study.practice.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class City {
    private Long id;
    private String name;
    private Long population;
    private List<Student> students;

    @Override
    public String toString() {
        return "City{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", population=" + population +
                ", students=" + students.size() +
                '}';
    }
}
