package ru.nlmk.study.practice.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Student {
    private Long id;
    private String firstName;
    private String lastName;
    private String middleName;
    private int age;
    private City city;
    private StudyClass studyClass;
}
