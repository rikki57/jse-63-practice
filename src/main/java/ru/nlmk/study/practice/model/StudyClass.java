package ru.nlmk.study.practice.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class StudyClass {
    private Long id;
    private String name;
    private List<Student> studentList;

    @Override
    public String toString() {
        return "StudyClass{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", studentList=" + studentList.size() +
                '}';
    }
}
