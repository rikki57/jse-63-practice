package ru.nlmk.study.practice.model.enums;

public enum Command {
    EXIT("exit"),
    CREATE("new"),
    DELETE("del"),
    GET("get"),
    UPDATE("update");

    private String commandAlias;

    Command(String commandAlias) {
        this.commandAlias = commandAlias;
    }

    public static Command findByAlias(String commandAlias) {
        for (Command v : values()) {
            if (v.getCommandAlias().equals(commandAlias)) {
                return v;
            }
        }
        return null;
    }

    public String getCommandAlias() {
        return commandAlias;
    }
}
