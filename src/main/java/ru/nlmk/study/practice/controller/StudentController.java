package ru.nlmk.study.practice.controller;

import ru.nlmk.study.practice.model.enums.Command;
import ru.nlmk.study.practice.service.StudentService;

public class StudentController {
    private final StudentService studentService = new StudentService();

    public String executeCommand(Command command, String[] params) {
        switch (command) {
            case CREATE:
                return studentService.addStudent(params).toString();
        }
        return "";
    }
}
