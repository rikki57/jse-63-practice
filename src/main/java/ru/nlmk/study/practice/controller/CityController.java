package ru.nlmk.study.practice.controller;

import ru.nlmk.study.practice.model.enums.Command;
import ru.nlmk.study.practice.service.CityService;

public class CityController {
    private final CityService cityService = new CityService();

    public String executeCommand(Command command, String[] params) {
        switch (command) {
            case CREATE:
                return cityService.addCity(params[0], Long.valueOf(params[1])).toString();
        }
        return "";
    }
}
