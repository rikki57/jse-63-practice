package ru.nlmk.study.practice.controller;

import ru.nlmk.study.practice.model.enums.Command;
import ru.nlmk.study.practice.service.StudyClassService;

public class StudyClassController {
    private final StudyClassService studyClassService = new StudyClassService();

    public String executeCommand(Command command, String[] params) {
        switch (command) {
            case CREATE:
                return studyClassService.addStudyClass(params[0]).toString();
        }
        return "";
    }
}
