package ru.nlmk.study.practice.repository;

import java.util.HashMap;
import java.util.Map;

public abstract class Repository<T> {
    protected final Map<Long, T> storage = new HashMap<>();

    public Long getNextId() {
        return storage.keySet().stream().mapToLong(v -> v).max().orElse(-1) + 1;
    }

    public T findById(Long id) {
        return storage.get(id);
    }

    public abstract T create(T input);

    public abstract T update(T input);

    public boolean delete(Long id) {
        if (storage.containsKey(id)) {
            storage.remove(id);
            return true;
        }
        return false;
    }
}
