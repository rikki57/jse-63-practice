package ru.nlmk.study.practice.repository;

import ru.nlmk.study.practice.model.City;

public class CityRepository extends Repository<City> {
    @Override
    public City create(City input) {
        storage.put(input.getId(), input);
        return input;
    }

    @Override
    public City update(City input) {
        storage.put(input.getId(), input);
        return input;
    }
}
