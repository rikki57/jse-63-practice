package ru.nlmk.study.practice.repository;

import ru.nlmk.study.practice.model.StudyClass;

public class StudyClassRepository extends Repository<StudyClass> {

    @Override
    public StudyClass create(StudyClass input) {
        storage.put(input.getId(), input);
        return input;
    }

    @Override
    public StudyClass update(StudyClass input) {
        storage.put(input.getId(), input);
        return input;
    }
}
