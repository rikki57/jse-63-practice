package ru.nlmk.study.practice.repository;

import ru.nlmk.study.practice.model.Student;

public class StudentRepository extends Repository<Student> {
    @Override
    public Student create(Student input) {
        storage.put(input.getId(), input);
        return input;
    }

    @Override
    public Student update(Student input) {
        storage.put(input.getId(), input);
        return input;
    }
}
