package ru.nlmk.study.practice;

import ru.nlmk.study.practice.controller.CityController;
import ru.nlmk.study.practice.controller.StudentController;
import ru.nlmk.study.practice.controller.StudyClassController;
import ru.nlmk.study.practice.model.enums.Command;
import ru.nlmk.study.practice.model.enums.Item;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    private static final StudentController studentController = new StudentController();
    private static final CityController cityController = new CityController();
    private static final StudyClassController studyClassController = new StudyClassController();

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        while (in.hasNextLine()) {
            String s = in.nextLine();
            String[] splited = s.split("\\s+");
            String result = "";
            Item item = Item.findByAlias(splited[0]);
            Command command = Command.findByAlias(splited[1]);
            switch (item) {
                case STUDENT:
                    result = studentController.executeCommand(command, Arrays.copyOfRange(splited, 2, splited.length));
                    break;
                case CITY:
                    result = cityController.executeCommand(command, Arrays.copyOfRange(splited, 2, splited.length));
                    break;
                case STUDY_CLASS:
                    result = studyClassController.executeCommand(command, Arrays.copyOfRange(splited, 2, splited.length));
                    break;
                default:
                    result = "Error executing command : not found!";
            }
            System.out.println(result);
            if (Command.EXIT.getCommandAlias().equals(s)) {
                break;
            }
        }
    }
}
