package ru.nlmk.study.practice.service;

import ru.nlmk.study.practice.model.City;
import ru.nlmk.study.practice.model.Student;
import ru.nlmk.study.practice.model.StudyClass;
import ru.nlmk.study.practice.repository.CityRepository;
import ru.nlmk.study.practice.repository.StudentRepository;
import ru.nlmk.study.practice.repository.StudyClassRepository;

public class StudentService {
    private final StudentRepository studentRepository = new StudentRepository();
    private final StudyClassRepository studyClassRepository = new StudyClassRepository();
    private final CityRepository cityRepository = new CityRepository();

    public Student addStudent(String[] params) {
        City city = cityRepository.findById(Long.valueOf(params[4]));
        StudyClass studyClass = studyClassRepository.findById(Long.valueOf(params[5]));
        if (city != null && studyClass != null) {
            Student student = Student.builder()
                    .firstName(params[0])
                    .middleName(params[1])
                    .lastName(params[2])
                    .age(Integer.parseInt(params[3]))
                    .city(city)
                    .studyClass(studyClass)
                    .build();
            studentRepository.create(student);
            city.getStudents().add(student);
            cityRepository.update(city);
            studyClass.getStudentList().add(student);
            studyClassRepository.update(studyClass);
            return student;
        }
        return null;
    }
}
