package ru.nlmk.study.practice.service;

import ru.nlmk.study.practice.model.City;
import ru.nlmk.study.practice.repository.CityRepository;

import java.util.ArrayList;

public class CityService {
    private final CityRepository cityRepository = new CityRepository();

    public City addCity(String name, Long population) {
        City city = City.builder()
                .name(name)
                .population(population)
                .students(new ArrayList<>())
                .id(cityRepository.getNextId())
                .build();
        City createdCity = cityRepository.create(city);
        return createdCity;
    }
}
