package ru.nlmk.study.practice.service;

import ru.nlmk.study.practice.model.StudyClass;
import ru.nlmk.study.practice.repository.StudyClassRepository;

import java.util.ArrayList;

public class StudyClassService {
    private final StudyClassRepository studyClassRepository = new StudyClassRepository();

    public StudyClass addStudyClass(String name) {
        StudyClass studyClass = StudyClass.builder()
                .name(name)
                .studentList(new ArrayList<>())
                .id(studyClassRepository.getNextId())
                .build();
        studyClassRepository.create(studyClass);
        return studyClass;
    }
}
